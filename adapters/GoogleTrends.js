const googleTrends 	= require('google-trends-api');
const Queue 		= require('../libs/queue.js');
const e       	   	= require('../libs/errors.js')

//const log     	  	= require('../libs/logger.js')('../log')

function moment(offset){
	let d = new Date()
	d = d.setDate(d.getDate()+offset)
	return new Date(d)
}




class AdptGoogleTrends extends Queue{
	constructor(slotsCount, keys){
		super(googleTrends, slotsCount, keys)
		console.log('Создан адаптер googleTrends')
	}

	iot (qTask){
		//console.log('Исполнение задания !!! - ' + qTask.task.task.task.name)

		qTask.key.adapter.interestOverTime({ 	keyword: qTask.twObj.job.request.keyword, 
												startTime: moment(qTask.twObj.job.request.depth),  							//new Date('2018.01.01'), 
												endTime: moment(0),								//new Date('2018.06.01'), 
												geo: qTask.twObj.job.request.geo, 
												granularTimeResolution : true}, (err, result)=>{
			try{
				var data = JSON.parse(result).default.timelineData.pop()
				var response = {
					success : true,
					data : result,
					value : data.value[0],
					description : data.formattedTime,
					date: new Date()
				}

			} catch(e){
				console.log(e)
				var response = {
					success : false,
					data: e,
					date: new Date()
				}
			} finally{
				qTask.twObj.handler(response)				
				this.releaseKey(qTask.key.num)					
			}
		})
		this.releaseSlot(qTask.slot)			
	}
}

module.exports = AdptGoogleTrends;






/*

class iot{
	constructor(param, parent){
		this.param = param
		this.parent = parent


		this.queue = new Queue(this)
		this.tasks = {};
	}

	run(task){
		let p_keyword = task.body.request.keyword;
		let p_startTime = moment(task.body.request.depth)
		let p_endTime = moment(0)
		let p_geo = task.body.request.geo

		googleTrends.interestOverTime({ keyword: p_keyword, 
										startTime: p_startTime, 
										endTime: p_endTime, 
										geo: p_geo, 
										granularTimeResolution : true}, (err, result)=>{

			if (err) throw new e.DataRequestError(err.message);
			try{	
				var data = JSON.parse(result).default
			} catch(err) {
				throw new e.ParsingError(e.message);
			}

			var last = data.timelineData.length-1

			if (last>-1){
				this.param.body.response_last = Object.assign({}, this.param.body.response) 					//Сохраняем предыдущее значение

				this.param.body.response.value = data.timelineData[last].value[0];						//Записываем новые
				this.param.body.response.description = data.timelineData[last].formattedAxisTime;
				this.param.body.response.date = new Date();


				//TODO Точка проверки динамики. Если да - делаем эмит
				if (analize.alert(this.param)){
					this.param.body.alerts.alert_last = Object.assign({}, this.param.body.alerts.alert_now) 
					this.param.body.alerts.alert_now = Object.assign({}, this.param.body.response) 

					this.parent.emit(this.param.body.task.event, this.param, this.getMessage())
				}

			} else {
				log.info('GoogleTrends no result')
			} 

			this.queue.next();

		})
	}
}

*/


//-----------------------------
function testRequest(tm, task, callback){
	var d = setTimeout(()=>{
		//console.log('таймер закончен')
		let err = false
		let data = task
		callback(err, data)
	}, tm)
}






/*
googleTrends.interestOverTime({ keyword: p_keyword, 
										startTime: p_startTime, 
										endTime: p_endTime, 
										geo: p_geo, 
										granularTimeResolution : true}, (err, result)=>{

			if (err) throw new e.DataRequestError(err.message);
			try{	
				var data = JSON.parse(result).default
			} catch(err) {
				throw new e.ParsingError(e.message);
			}

			var last = data.timelineData.length-1

			if (last>-1){
				this.param.body.response_last = Object.assign({}, this.param.body.response) 					//Сохраняем предыдущее значение

				this.param.body.response.value = data.timelineData[last].value[0];						//Записываем новые
				this.param.body.response.description = data.timelineData[last].formattedAxisTime;
				this.param.body.response.date = new Date();


				//TODO Точка проверки динамики. Если да - делаем эмит
				if (analize.alert(this.param)){
					this.param.body.alerts.alert_last = Object.assign({}, this.param.body.alerts.alert_now) 
					this.param.body.alerts.alert_now = Object.assign({}, this.param.body.response) 

					this.action(this.param)
					
				}

			} else {
				//log.info('GoogleTrends no result')
			} 

			this.releaseQueue;

		})
*/