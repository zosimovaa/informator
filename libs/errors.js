var util    = require('util')
//var log     	   = require('./logger.js')('../log')


//Ошибка парсинга 
function ParsingError(message){
	this.message=message;
//	log.error('Throwing ParsingError')
//	log.error(message)
	Error.captureStackTrace(this, ParsingError)
}
util.inherits(ParsingError, Error)
ParsingError.prototype.name = 'ParsingError'

//Ошибка получения данных
function DataRequestError(message){
	this.message=message;
//	log.error('Throwing DataRequestError')
//	log.error(message)
	Error.captureStackTrace(this, DataRequestError)
}
util.inherits(DataRequestError, Error)
DataRequestError.prototype.name = 'DataRequestError'



//Логическая ошибка данных
function LogicDataError(message){
	this.message=message;
//	log.error('Throwing LogicDataError')
//	log.error(message)
	Error.captureStackTrace(this, LogicDataError)
}
util.inherits(LogicDataError, Error)
LogicDataError.prototype.name = 'LogicDataError'






//=================================================================================
module.exports.ParsingError = ParsingError
module.exports.DataRequestError = DataRequestError
module.exports.LogicDataError = LogicDataError