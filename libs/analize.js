/*
Из таска берутся 
 - текущее значение, из response
 - дата последнего значения
 - значение прошлого алерта
 - дата прошлого алерта
 - условия
 - таймаут

*/

function alert(obj){
	
	let value = obj.response.value;
	let value_date = obj.response.date;
	let value_last = obj.alert.last.value;
	let value_last_date = obj.alert.last.date;
	let params = obj.alert.conditions.alert;
	let timeout = obj.alert.conditions.timeout;

	let alert_or = false;

	params.forEach((item)=>{
		//Цикл проверки на уровне ИЛИ
		let alert_and = true
		for (key in item) {
			//Цикл проверки на уровне И
			//item[key] - Пороговое значение
			switch(key){
				//Условие <больше>
				case 'more':
					alert_and = value >= item[key] ? alert_and : false;
					break;
				//Условие <меньше>
				case 'less':
					alert_and = value <= item[key] ? alert_and : false;
					break;
				//Условие <изменение значение на x%>
				case 'rel':
					//TODO Сделать проверку на ноль и нормальный запуск при инициализации (на старте будет NULL)
					alert_and = Math.round(100*Math.abs(value-value_last)/value_last,0) >= item[key] ? alert_and : false;
					break;
				//Отличие по модулю
				case 'diff_abs':
					alert_and = Math.abs(value-value_last) >= item[key] ? alert_and : false;
					break;

				default:
					alert_and = false;
					break;
			}
		}

		if (alert_and){
			alert_or = true;
		}
	})

	//Проверка по таймауту на уведомление
	if (alert_or){
		if ((value_date - value_last_date)/1000 > timeout) {
			return true
		}
	}
	return false
}



//===================================================
module.exports.alert = alert;