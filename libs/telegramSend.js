/*
Модуль отправки в телеграм.
v 1.1 - обновленный спамконтроль
@param {STRING} api - апи ключ telegram
@param {Integer} spamCountStop - ограничение по количеству собщений (Если не указано, то спам контроль отключен)
@param {Integer} spamTimeout - ограничение по времени в секундах (Если не указано, то принимаем окно равным 60 секундам)

@returns 0 - ok, 1 - spam blocked


===========================================================================================================
*/

var https = require('https');
var EventEmitter = require('events').EventEmitter;
//------------------------------------------------------------

class TelegramSend extends EventEmitter {
  constructor(api, spamCountStop, spamTimeout) {
    super();
    console.log('Инициализация отправки в телеграм');
    this.spamCountStop = spamCountStop || 0;
    this.spamCount = 0
    this.spamTimeout = spamTimeout || 60
    this.spamDate = new Date();
    this.blockedCount = 0;
    this.url = 'https://api.telegram.org/bot'+ api +'/sendMessage?chat_id='// + this.chatID + '&text='
    //console.log('Ограничение по количеству сообщений ', this.spamCountStop);
    //console.log('Ширина временного окна для отправки ', this.spamTimeout);
  }

  send(chatID, msg){
    //блок спам контроля
    this.spamCount++;
    var now = new Date();
    if (this.spamCount>this.spamCountStop && this.spamCountStop) {    //мы превысили порог отправки и порог отправки задан
      //мы превысили порог отправки
      if ((now - this.spamDate)/1000>this.spamTimeout){
        //при этом временной интервал истек и можно шарашить дальше
        this.spamDate = now;
        this.spamCount = 0;
        this.message(chatID, 'Спам контроль. Сообщений блокировано: ' + this.blockedCount)
        this.blockedCount = 0;
      } else {
        this.blockedCount++
        return (1);
      }
    }
    this.message(chatID, msg);
    return(0);
  }

  message(chatID, msg){
    var request = https.get(this.url + chatID + '&text=' + Utf8.encode(msg), (res) => {   
      res.setEncoding('utf8');
      res.on('data', (body)=>{
        const result = JSON.parse(body);
        if (result.ok==true) return 0;
        this.emit('error', result);
      });
    });
    request.on('error', (err)=>{
      this.emit('error', err);
    });
  }
}

module.exports = TelegramSend;


//=============================================================================
var Utf8 = {
    // public method for url encoding
    encode : function (string) {
        string = string.replace(/rn/g,"n");
        var utftext = "";
        for (var n = 0; n < string.length; n++) {
            var c = string.charCodeAt(n);
            if (c < 128) {
                utftext += String.fromCharCode(c);
            }
            else if((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            }
            else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }
        }
        return utftext;
    },

    // public method for url decoding
    decode : function (utftext) {
        var string = "";
        var i = 0;
        var c = c1 = c2 = 0;
        while ( i < utftext.length ) {
            c = utftext.charCodeAt(i);
            if (c < 128) {
                string += String.fromCharCode(c);
                i++;
            }
            else if((c > 191) && (c < 224)) {
                c2 = utftext.charCodeAt(i+1);
                string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                i += 2;
            }
            else {
                c2 = utftext.charCodeAt(i+1);
                c3 = utftext.charCodeAt(i+2);
                string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                i += 3;
            }
        }
        return string;
    }
}