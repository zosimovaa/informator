var fs 			  = require('fs');
var winston 	= require('winston'); 

const DATE_TIME_FORMAT = {
    year: 'numeric',
    month: '2-digit',
    day: '2-digit',
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit'
};

var tsFormat = () => (new Date()).toLocaleString('ru' , DATE_TIME_FORMAT);
//==================================================================================

module.exports = function(dir){
    var options = {
      file: {
        level: 'info',
        timestamp: tsFormat,
        filename: `${dir}/info.log`,
        handleExceptions: false,
        json: true,
        maxsize: 5242880, // 5MB
        maxFiles: 5,
        colorize: false,
      },
      console: {
        level: 'info',
        timestamp: tsFormat,
        handleExceptions: false,
        json: false,
        colorize: false,
      },
    };

    var logger = new (winston.Logger)({
      transports: [
        new winston.transports.File(options.file),
        new winston.transports.Console(options.console)
      ],
      exitOnError: false, // do not exit on handled exceptions
    });

    LOGDIR = dir
    if (!fs.existsSync(LOGDIR)) {
        fs.mkdirSync(LOGDIR);
    }
  return logger
}


