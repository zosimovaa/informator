var EventEmmiter     = require('events');
var guid             = require('./guid.js');
/*
*
* Методы
* - addTask
* - 
*
*
*
*
*/


function testRequest(value, callback){
	var t = setTimeout(()=>{
		if (value){
			callback(null, value)
		} else {
			callback('error', value)
		}
	}, 2000)
}







class Queue extends EventEmmiter{
	constructor(adapter, slotsCount, keys){
		super()
		this.slots={};              //количество слотов для работы модуля
	    this.keys = {};             //объект выпоняющий запрос
	    this.delay = 1000/slotsCount;     //задержка
	    this.queue = [];            //очередь заданий [func, args, event]
	    this.status = 'init';
	    //------------------------------------------
	    
	    //Инициализация слотов
	    while (slotsCount){
	      this.slots[slotsCount]='ready';
	      slotsCount--;
	    }
	    
	    //Инициализация ключей
	    for (var i in keys){
	      this.keys[i]={
	        adapter  : adapter,			//Здесь при необходимости инициализация с использованием ключа. 
	        status   : 'ready',
	        num      : i
	      }
	    }
	    this.setStatus('ok');
	}


	addTask(func, task, id, attempt){

		if (!this[func]) return 0

		let qTask = {                 			// Объект задания
			id     : id || guid(),    			// Если id передан, то используем его (повтор), иначе генерим новый (новое задание в очереди) 
	      	func   : func,            			// Название функции для запуска
	      	twObj  : task,						// Объект заданий, который передается в функцию
	      	key    : null,
	      	slot   : null,
	      	attempt: attempt+1 || 1, 		// Если номер попытки передан, то +1 (повтор), иначе начинаем с 1 (новое задание в очереди)
	      	error  : null 						// Объект ошибки
	    }
	    this.queue.push(qTask);
	    //console.log('queue Задание добавлено ' + func)
	    this.action();
	    return qTask.id;
	}

	//Удаление задания по его ID
	delTask(id){
		//not implemented yet
		return -1
	}

	//Изменение статуса очереди
	setStatus(status){
		this.status = status;
    	this.action();
	}


	action(){
		setImmediate(()=>{
	    let slot = null;
	    let key = null;
	    if (this.queue.length>0 && this.status=='ok'){
	      	slot = this.getSlot();
	      	if (slot){
	        	key = this.getKey();
	        	if (key){
	        		//console.log('Взят слот ',  slot)
	        		//console.log('Взят ключ ',  key)
	        		let qTask = this.queue.shift();
	        		qTask.slot = slot
	        		qTask.key = this.keys[key]; 
	          		this[qTask.func](qTask);
	        	} else {
	          		this.slots[slot] = 'ready';
	        	}
	      	}
	    }
	    })
	}


	getSlot(){
    	for (var key in this.slots){
      		if (this.slots[key]=='ready'){
      			//console.log(this.slots)
        		//console.log('Взят слот ',  key)
        		this.slots[key] = 'busy'
        		//console.log(this.slots)
        		//console.log('---------------------------')
        		return key
      		}
    	}
    	return 0;
  	}

  
  	getKey(){
    	for (var key in this.keys){
      		if (this.keys[key].status=='ready'){
    	    	//console.log('Взят ключ ',  key);
        		this.keys[key].status = 'busy';
        		return key;
      		}
    	}
    	return 0;
  	}

  	releaseSlot(s, imm){
    	var t = setTimeout(()=>{
        	//console.log('Освобождение слота ', s);
        	this.slots[s] = 'ready';
        	this.action();
      	},this.delay)
  	}

  	releaseKey(k){
    	//console.log('Освобождение ключа ', k);
    	this.keys[k].status = 'ready';
    	this.action();
  	}

  	testFunc(param){
  		param.key.adapter(param.twTask,(err, data)=>{

  			if (err) console.log(err)
  			if (data)console.log(data)
  			this.releaseKey(param.key.num)	

  		})
  		this.releaseSlot(param.slot)

  	}


}


module.exports = Queue;

/*
var keys = {1: 'key1', 2: 'key2', 3:'key3'}


var d = new Queue(testRequest, 2, keys)

d.addTask('testFunc1', 'some value0')
d.addTask('testFunc', 'some value1')
d.addTask('testFunc', 'some value2')
d.addTask('testFunc', 'some value3')
d.addTask('testFunc', 'some value4')
d.addTask('testFunc', 'some value5')


*/





