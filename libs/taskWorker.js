const analize 		= require('../libs/analize.js')





class TaskWorker {
	constructor(task, adpt, snd){
		this.status = 'no_init'
		this.job = Object.assign({},task)
		this.adpt = adpt
		this.tm = task.task.scheduler.timeout
		this.tmr = null
		this.snd = snd;
	}

	run(){
		this.tmr = setInterval(this.execute.bind(this), this.tm * 1000);
		this.execute()
	}

	stop(){
		clearInterval(this.tmr);
	}

	execute(){
		let id = this.adpt.addTask(this.job.task.method, this);
		console.log('Задание добавлено  из воркера ' + id)
	}

	handler(result){
		console.log('=============================================')
		//console.log(result)

		if (result.success){
			this.job.response = Object.assign({}, result)

			console.log('Полученое значение: ' + this.job.response.value)
			if (analize.alert(this.job)){
				console.log('ALERT!!!!')
				//console.log(this.job.user.chatId)
				//console.log(this.job.alert.message)
				this.snd.message(this.job.user.chatId, this.messageBuilder())
				this.job.alert.last = Object.assign({}, this.job.response) 



				//this.parent.emit(this.param.body.task.event, this.param, this.getMessage())
			}
		} else {
			//Ошибка при получении данных
			console.log('Ошибка при получении данных')
		}
	}

	messageBuilder(){
		let msg = this.job.alert.message;
		let dyn = Math.round((1-this.job.response.value/this.job.alert.last.value),0)
		msg=msg.replace('@name', this.job.task.name);
		msg=msg.replace('@value', this.job.response.value);
		msg=msg.replace('@dynamic', dyn);
		return msg;
	}
}

module.exports = TaskWorker





















/*
class QT{
	constructor(){

	}

	addTask(t){
		//t.handler(t.task)
		//let f = setTimeout(()=>{t.handler(t.task)}, 10000)

		let f = setTimeout(t.handler.bind(t, t.task), 100)
		
	}
}


qt = new QT()

th = new TaskWorker("eeee", qt)
th.run()


setTimeout(()=>{th.stop()},3200)

*/