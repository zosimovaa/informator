module.exports = {
	id		: null,
	task 	: {
				name       : 'Грипп',
				description: 'Активность поиска по теме ГРИППа',
				class      : 'adptGoogleTrends',
				method     : 'iot',
				scheduler  : {
					next     : new Date(),					
					timeout  : 5						//Seconds!
				},
			},
	request : {
				keyword : 'грипп',
				geo     : 'RU',
				depth   : -365
			},
	response: {
				success : true,
				data    : null, 
				value   : 0,
				description : null,
				date    : null
			},
	alert 	: {
				conditions : {
					alert    : [{more: 1, less: 100}],
					timeout  : 16							//Seconds
				},
				last: {
					value      : 0,
					description: null,
					date       : null
				},
				message: "GoogleTrends. @name.\nИнтерес к теме: @value (@dynamic%)"
			},
	user 	: {
				chatId : 211945135,
				name   : 'Alex Zosimov'
	},
}
