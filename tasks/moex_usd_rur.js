module.exports = {
		success  : true, 
		body     : {
			task   : {
				system     : 'MOEX',
				name       : 'USD-RUR',
				description: 'Курс доллара',
				class      : 'AdptMOEX',
				method     : 'getRate',
				id         : 2,
				event      : 'data',
				sheduler   : {
					next     : new Date(),
					timeout  : 900,
					timer    : null
				},
			}, 
			request  : {
				keyword    : 'USD/RUB',
				geo        : 'RU',
				depth      : -60
			},
			response : {
				value      : 0,
				description: null,
				date       : null
			},
			response_last: {
				value      : 0,
				description: null,
				date       : null
			},
			alerts : {
				isNull     : false,
				conditions : {
					alert    : [{rel: 1}],
					timeout  : 0
				},
				alert_now : {
					value      : 0,
					description: null,
					date       : null
				},
				
				alert_last : {
					value      : 0,
					description: null,
					date       : null
				}
			}
		},
		message:''
	}