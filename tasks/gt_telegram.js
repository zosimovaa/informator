module.exports = {
		success  : true, 
		body     : {
			task   : {
				system     : 'Google Trends',
				name       : 'Telegram',
				description: 'Информирует о популярности телеграма в поиске',
				class      : 'AdptGoogleTrends',
				method     : 'iot',
				id         : 1,
				event      : 'data',
				sheduler  : {
					next     : new Date(),
					timeout  : 900,
					timer    : null
				},
			},
			request  : {
				keyword : 'telegram',
				geo     : 'RU',
				depth   : -100
			},
			response : {
				value   : 0,
				description : null,
				date    : null
			},
			response_last: {
				value      : 0,
				description: null,
				date       : null
			},
			alerts : {
				isNull     : false,
				conditions : {
					alert    : [{more: 5, less: 100}],
					timeout  : 1200
				},
				alert_now : {
					value      : 0,
					description: null,
					date       : null
				},
				
				alert_last : {
					value      : 0,
					description: null,
					date       : null
				}
			}
		},
		message:''
	}