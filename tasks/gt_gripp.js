module.exports = {
		success  : true, 
		body     : {
			task   : {
				system     : 'Google Trends',
				name       : 'Грипп',
				description: 'Активность поиска по теме ГРИППа',
				class      : 'AdptGoogleTrends',
				method     : 'iot',
				id         : 1,
				event      : 'data',
				sheduler  : {
					next     : new Date(),
					timeout  : 900,
					timer    : null
				},
			},
			request  : {
				keyword : 'грипп',
				geo     : 'RU',
				depth   : -365
			},
			response : {
				value   : 0,
				description : null,
				date    : null
			},
			response_last: {
				value      : 0,
				description: null,
				date       : null
			},
			alerts : {
				isNull     : false,
				conditions : {
					alert    : [{more: 5, less: 100}],
					timeout  : 1800
				},
				alert_now : {
					value      : 0,
					description: null,
					date       : null
				},
				
				alert_last : {
					value      : 0,
					description: null,
					date       : null
				}
			}
		},
		message:''
	}